from django.shortcuts import render
from django.shortcuts import render, redirect
from account.decorators import allowed_user, unauthenticated_user
from django.contrib.auth.decorators import login_required

from .models import AnnouncementModel
from .forms import AnnouncementForm
from django.utils import timezone

# Create your views here.

@login_required(login_url='/account/login/')
def AnnView(request,idAn):
    Ann = AnnouncementModel.objects.get(id=idAn)
    context = {
        'Ann':Ann,
        'User':request.user,
    }
    return render(request,'announcement/viewAn.html',context)

@login_required(login_url='/account/login/')
@allowed_user(allowed_roles=['lecturer'])
def AnnAdd(request):
    AnnForm = AnnouncementForm(request.POST)
    context = {
        'AnnForm':AnnForm,
    }

    if request.method == 'POST':
        if AnnForm.is_valid():
            Ann = AnnForm.save(commit=False)
            Ann.publisherAn = request.user
            Ann.save()
            return redirect('/dashboard')

    return render(request,'announcement/formAn.html',context)

@login_required(login_url='/account/login/')
@allowed_user(allowed_roles=['lecturer'])
def AnnEdit(request,idAn):
    Ann = AnnouncementModel.objects.get(id=idAn)
    data = {
        'judulAn'   : Ann.judulAn,
        'isiAn'     : Ann.isiAn,
    }

    AnnForm = AnnouncementForm(request.POST or None, initial=data, instance=Ann)
    context = {
        'AnnForm':AnnForm,
    }

    if request.method == 'POST':
        if AnnForm.is_valid():
            AnnForm.save()
            return redirect('/dashboard')
    
    return render(request,'announcement/formAn.html',context)


@login_required(login_url='/account/login/')
@allowed_user(allowed_roles=['lecturer'])
def AnnDelete(request,idAn):
    AnnouncementModel.objects.filter(id=idAn).delete()
    return redirect('/dashboard')