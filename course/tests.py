from django.test import TestCase, Client
from .models import Course, Week, Enrollment
from account.models import Account
from django.urls import resolve, reverse
from .views import course, enroll, unenroll, success, coursepage, search
from account.models import Account
from django.contrib.auth.models import Group

# Create your tests here.
class TestWeek(TestCase):

    #add week
    def test_url_addweekform_exist(self):
        c=Client()

        course = Course(name='dummycourse')
        course.save()

        user = Account.objects.create(nomorinduk='190639', date_of_birth='2000-01-01')
        user.set_password('12345')
        user.save()
        Group.objects.create(name='lecturer')
        group = Group.objects.get(name='lecturer')
        user.groups.add(group)

        c.login(username='190639', password='12345')

        course_obj = Course.objects.get(id=1)
        course_obj.member.add(user)
        course_obj.save()

        # post_response = c.post(reverse('course:add-week', args=(course.id,)), follow=True)
        post_response = c.get('/course/add-week/1')
        self.assertEquals(post_response.status_code, 200)

    def test_complete_addweekform(self):
        c=Client()

        course = Course(name='dummycourse')
        course.save()

        user = Account.objects.create(nomorinduk='190639', date_of_birth='2000-01-01')
        user.set_password('12345')
        user.save()
        Group.objects.create(name='lecturer')
        group = Group.objects.get(name='lecturer')
        user.groups.add(group)

        c.login(username='190639', password='12345')

        course_obj = Course.objects.get(id=1)
        course_obj.member.add(user)
        course_obj.save()
        
        response = c.get('/course/add-week/1')
        html_kembalian = response.content.decode('utf8')
        self.assertIn("Week Form", html_kembalian)
        self.assertIn('Please include the whole link with "https://"', html_kembalian)

    def test_template_addweekform_exist(self):
        c=Client()

        course = Course(name='dummycourse')
        course.save()

        user = Account.objects.create(nomorinduk='190639', date_of_birth='2000-01-01')
        user.set_password('12345')
        user.save()
        Group.objects.create(name='lecturer')
        group = Group.objects.get(name='lecturer')
        user.groups.add(group)

        c.login(username='190639', password='12345')

        course_obj = Course.objects.get(id=1)
        course_obj.member.add(user)
        course_obj.save()

        response = c.get('/course/add-week/1')
        self.assertTemplateUsed(response, 'course/add-week.html')

    # update Week
    def test_url_updateweekform_exist(self):
        c=Client()

        course = Course(name='dummycourse')
        course.save()

        user = Account.objects.create(nomorinduk='190639', date_of_birth='2000-01-01')
        user.set_password('12345')
        user.save()
        Group.objects.create(name='lecturer')
        group = Group.objects.get(name='lecturer')
        user.groups.add(group)

        c.login(username='190639', password='12345')

        course_obj = Course.objects.get(id=1)
        course_obj.member.add(user)
        course_obj.save()
        week = Week(course=course)
        week.save()

        response = c.get('/course/update-week/1')
        self.assertEquals(response.status_code, 200)

    def test_complete_updateweekform(self):
        c=Client()

        course = Course(name='dummycourse')
        course.save()

        user = Account.objects.create(nomorinduk='190639', date_of_birth='2000-01-01')
        user.set_password('12345')
        user.save()
        Group.objects.create(name='lecturer')
        group = Group.objects.get(name='lecturer')
        user.groups.add(group)

        c.login(username='190639', password='12345')

        course_obj = Course.objects.get(id=1)
        course_obj.member.add(user)
        course_obj.save()

        week = Week(course=course)
        week.save()

        response = c.get('/course/update-week/1')
        html_kembalian = response.content.decode('utf8')
        self.assertIn("Week Form", html_kembalian)
        self.assertIn('Please include the whole link with "https://"', html_kembalian)

    def test_template_updateform_exist(self):
        c=Client()

        course = Course(name='dummycourse')
        course.save()

        user = Account.objects.create(nomorinduk='190639', date_of_birth='2000-01-01')
        user.set_password('12345')
        user.save()
        Group.objects.create(name='lecturer')
        group = Group.objects.get(name='lecturer')
        user.groups.add(group)

        c.login(username='190639', password='12345')

        course_obj = Course.objects.get(id=1)
        course_obj.member.add(user)
        course_obj.save()

        week = Week(course=course)
        week.save()

        response = c.get('/course/update-week/1')
        self.assertTemplateUsed(response, 'course/add-week.html')

class TestModel(TestCase):
    def setUp(self):
        course = Course(name='dummycourse')
        course.save()
        week = Week(course=course)
        week.save()

    def test_model_exist(self):
        count_courses = Course.objects.all().count()
        self.assertEquals(count_courses, 1)
        count_week = Week.objects.all().count()
        self.assertEquals(count_week, 1)

    def test_course_toString(self):
        course = Course(name='dummycourse')
        self.assertTrue(isinstance(course, Course))
        self.assertEquals(course.__str__(), course.name)

class TestListEnrollment(TestCase):
    def test_event_url_is_exist(self):
        c = Client()
        user = Account.objects.create(nomorinduk='12345', date_of_birth='2005-10-10')
        user.set_password('12345')
        user.save()
        c.login(nomorinduk='12345', password='12345')
        response = c.get('/course/list/')
        self.assertEqual(response.status_code, 200)

    def test_event_func(self):
        found_func = resolve('/course/list/')
        self.assertEqual(found_func.func, course)

    def test_event_using_template(self):
        c = Client()
        user = Account.objects.create(nomorinduk='12345', date_of_birth='2005-10-10')
        user.set_password('12345')
        user.save()
        c.login(nomorinduk='12345', password='12345')
        template = c.get('/course/list/')
        self.assertTemplateUsed(template, 'course/list-matkul.html')

class TestSearch(TestCase):
    def test_event_url_is_exist(self):
        c = Client()
        user = Account.objects.create(nomorinduk='12345', date_of_birth='2005-10-10')
        user.set_password('12345')
        user.save()
        c.login(nomorinduk='12345', password='12345')
        response = c.get('/course/search/?q=ppw')
        self.assertEqual(response.status_code, 200)

    def test_event_func(self):
        found_func = resolve('/course/search/')
        self.assertEqual(found_func.func, search)

    def test_event_using_template(self):
        c = Client()
        user = Account.objects.create(nomorinduk='12345', date_of_birth='2005-10-10')
        user.set_password('12345')
        user.save()
        c.login(nomorinduk='12345', password='12345')
        template = c.get('/course/list/')
        self.assertTemplateUsed(template, 'course/list-matkul.html')

class TestEnrollment(TestCase):
    def test_event_func(self):
        found_func = resolve('/course/enroll/1/')
        self.assertEqual(found_func.func, enroll)

    def test_event_count_enrollment(self):
        c = Client()

        #making initial course
        course = Course(name="PPW")
        course.save()

        #register dummy user
        user = Account.objects.create(nomorinduk='12345', date_of_birth='2005-10-10')
        user.set_password('12345')
        user.save()

        #user login
        logged_in = c.login(nomorinduk='12345', password='12345')

        #call views
        response = c.post(reverse('course:enroll', args=(course.id,)), follow=True)
        self.assertEqual(Enrollment.objects.all().count(), 1)

class TestUnenroll(TestCase):
    def test_unenroll_url(self):
        c = Client()

        #making initial course
        course = Course(name="PPW")
        course.save()

        #register dummy user
        user = Account.objects.create(nomorinduk='12345', date_of_birth='2005-10-10')
        user.set_password('12345')
        user.save()


        #user login
        c.login(nomorinduk='12345', password='12345')

        #enrollment
        course_obj = Course.objects.get(id=1)
        course_obj.member.add(user)
        course_obj.save()

        #call views
        response = c.post(reverse('course:unenroll', args=(course_obj.id,)), follow=True)
        self.assertEqual(Enrollment.objects.all().count(), 0)

    def test_event_func(self):
        found_func = resolve('/course/unenroll/1/')
        self.assertEqual(found_func.func, unenroll)

class TestSuccess(TestCase):

    def test_unenroll_url_post_is_exist(self):
        c = Client()
        user = Account.objects.create(nomorinduk='12345', date_of_birth='2005-10-10')
        user.set_password('12345')
        user.save()
        c.login(nomorinduk='12345', password='12345')
        response = c.get('/course/success/')
        self.assertEqual(response.status_code, 200)

    def test_event_func(self):
        c = Client()
        user = Account.objects.create(nomorinduk='12345', date_of_birth='2005-10-10')
        user.set_password('12345')
        user.save()
        c.login(nomorinduk='12345', password='12345')
        found_func = resolve('/course/success/')
        self.assertEqual(found_func.func, success)

    def test_event_using_template(self):
        c = Client()
        user = Account.objects.create(nomorinduk='12345', date_of_birth='2005-10-10')
        user.set_password('12345')
        user.save()
        c.login(nomorinduk='12345', password='12345')
        template = c.get('/course/success/')
        self.assertTemplateUsed(template, 'course/success.html')

class TestCoursePage(TestCase):
    #add week
    def test_url_coursepage_exist(self):
        c=Client()

        course = Course(name='dummycourse')
        course.save()

        user = Account.objects.create(nomorinduk='190639', date_of_birth='2000-01-01')
        user.set_password('12345')
        user.save()

        c.login(username='190639', password='12345')

        course_obj = Course.objects.get(id=1)
        course_obj.member.add(user)
        course_obj.save()

        # post_response = c.post(reverse('course:add-week', args=(course.id,)), follow=True)
        post_response = c.get('/course/detail/1/')
        self.assertEquals(post_response.status_code, 200)

    def test_complete_addweekform(self):
        c=Client()

        course = Course(name='dummycourse')
        course.save()

        user = Account.objects.create(nomorinduk='190639', date_of_birth='2000-01-01')
        user.set_password('12345')
        user.save()
        Group.objects.create(name='lecturer')
        group = Group.objects.get(name='lecturer')
        user.groups.add(group)

        c.login(username='190639', password='12345')

        course_obj = Course.objects.get(id=1)
        course_obj.member.add(user)
        course_obj.save()
        
        response = c.get('/course/detail/1/')
        html_kembalian = response.content.decode('utf8')
        self.assertIn("Course Information", html_kembalian)
        self.assertIn("Syllabus Document", html_kembalian)
        self.assertIn("Lecturer", html_kembalian)

    def test_template_addweekform_exist(self):
        c=Client()

        course = Course(name='dummycourse')
        course.save()

        user = Account.objects.create(nomorinduk='190639', date_of_birth='2000-01-01')
        user.set_password('12345')
        user.save()
        Group.objects.create(name='lecturer')
        group = Group.objects.get(name='lecturer')
        user.groups.add(group)

        c.login(username='190639', password='12345')

        course_obj = Course.objects.get(id=1)
        course_obj.member.add(user)
        course_obj.save()

        response = c.get('/course/detail/1/')
        self.assertTemplateUsed(response, 'course/course-detail.html')