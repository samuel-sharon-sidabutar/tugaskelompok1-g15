from django.urls import path
from . import views
# from .views import CourseDetailView
# from .views import AddWeekView, UpdateWeekView

app_name = 'course'

urlpatterns = [
    path('add-week/<int:pk>', views.add_week , name='add-week'),
    path('update-week/<int:pk>', views.update_week, name='update-week'),
    path('list/', views.course, name='listcourse'),
    path('search/', views.search, name='search'),
    path('enroll/<int:id_course>/', views.enroll, name='enroll'),
    path('success/', views.success, name='success'),
    path('unenroll/<int:id_course>/', views.unenroll, name='unenroll'),
    path('detail/<int:id_enroll>/', views.coursepage, name='course_page')
]  