from django.db import models
from django.urls import reverse
from TK1G15PJJ1.settings import AUTH_USER_MODEL as User
import datetime

# Create your models here.
# class Student(models.Model):
#     name = models.CharField(max_length=256)

#     def __str__(self):
#         return self.name

class Course(models.Model):
    name = models.CharField(max_length=256)
    syllabus = models.CharField(max_length=256, default='-')
    description = models.TextField(default='no desc')
    member = models.ManyToManyField(User, through='Enrollment')
    image = models.CharField(max_length=256, default='coding.png')

    def __str__(self):
        return self.name

class Week(models.Model):
    topic = models.TextField(default='Read the syllabus')
    google_meet_link = models.CharField(max_length=256, default='https://docs.google.com/document/d/1ylU3ZOxiG38ORD9em_8GTQ6ZacgFwQ96KQOTbpygZTI/edit?usp=sharing')
    homework_drive  = models.CharField(max_length=256, default='https://docs.google.com/document/d/1ylU3ZOxiG38ORD9em_8GTQ6ZacgFwQ96KQOTbpygZTI/edit?usp=sharing')
    quiz_forms = models.CharField(max_length=256, default='https://docs.google.com/document/d/1ylU3ZOxiG38ORD9em_8GTQ6ZacgFwQ96KQOTbpygZTI/edit?usp=sharing')
    topic_sheets = models.CharField(max_length=256, default='https://docs.google.com/document/d/1ylU3ZOxiG38ORD9em_8GTQ6ZacgFwQ96KQOTbpygZTI/edit?usp=sharing')
    course = models.ForeignKey(Course, on_delete=models.CASCADE)

class Enrollment(models.Model):
    member = models.ForeignKey(User, on_delete=models.CASCADE)
    course = models.ForeignKey(Course, on_delete=models.CASCADE)
    date_enrollment = models.DateField(default = datetime.date.today)

    class Meta:
        unique_together = [['member','course']]
